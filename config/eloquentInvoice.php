<?php

return [
    'default_currency' => 'EUR',
    'default_status' => 'draft',
    'locale' => 'fr_FR',
    'default_sender' => 'John DOE, 1 rue de la Paroisse, 78000 Versailles',
    'default_tax' => '0.2'
];
