# eloquent-invoice

![Latest Version on Packagist][ico-version]
 [![Software License][ico-license]](LICENSE.md)
![Total Downloads][ico-downloads]

Easy invoice creation for Laravel 5.4 and up. Unlike Laravel Cashier, this package is payment gateway agnostic.
This package is inspired by https://github.com/sandervanhooft/laravel-invoicable, but i needed to link invoicable model to invoice lines.

## Install

Via Composer

``` bash
$ composer require eric-tromas/eloquent-invoice
```

Next, you must install the service provider if you work with Laravel 5.4:

``` php
// config/app.php
'providers' => [
    ...
    EricTromas\EloquentInvoice\EloquentInvoiceServiceProvider::class,
];
```

You can publish the migration with:

``` bash
$ php artisan vendor:publish --provider="EricTromas\EloquentInvoice\EloquentInvoiceServiceProvider" --tag="migrations"
```

After the migration has been published you can create the invoices and invoice_lines tables by running the migrations:

``` bash
$ php artisan migrate
```

Optionally, you can also publish the `eloquentInvoice.php` config file with:

``` bash
$ php artisan vendor:publish --provider="EricTromas\EloquentInvoice\EloquentInvoiceServiceProvider" --tag="config"
```

If you'd like to override the design of the invoice blade view and pdf, publish the view:

``` bash
$ php artisan vendor:publish --provider="EricTromas\EloquentInvoice\EloquentInvoiceServiceProvider" --tag="views"
```


You can now edit `receipt.blade.php` in `<project_root>/resources/views/eloquentInvoice/receipt.blade.php` to match your style.

__Translations are ready in french and english__

$ php artisan vendor:publish --provider="EricTromas\EloquentInvoice\EloquentInvoiceServiceProvider" --tag="translations"

## Usage

__Money figures are in cents!__

Add the invoicable trait to the Eloquent model which represent an invoice line:

``` php
use Illuminate\Database\Eloquent\Model;
use EricTromas\EloquentInvoice\IsInvoicable\IsInvoicableTrait;

class Order extends Model
{
    use IsInvoicableTrait; // enables the ->invoices() Eloquent relationship
}
```

Now you can create invoices for an Order:

``` php
$invoice = new Invoice();

// Set additional information (optional)
$invoice->currency; // defaults to 'EUR' (see config file)
$invoice->status; // defaults to 'draft' (see config file)
$invoice->receiver_info; // defaults to null
$invoice->sender_info; // defaults to null
$invoice->payment_info; // defaults to null
$invoice->note; // defaults to null

$invoice->save();

// To add a line to the invoice, use these example parameters:
$invoice = $invoice->addLine($invoicableModel, $invoicableId, $priceInCents, $description, $quantity, $tax = 0.20);

// Invoice totals are now updated
echo $invoice->total; // 242
echo $invoice->tax; // 42


// access individual invoice lines using Eloquent relationship
$invoice->lines;
$invoice->lines();

// Access as pdf
$invoice->download(); // download as pdf (returns http response)
$invoice->pdf(); // or just grab the pdf (raw bytes)

// Handling discounts by adding a line with a negative amount.

// Convenience methods
Invoice::findByReference($reference);
Invoice::findByReferenceOrFail($reference);
$invoiceLine->invoicable() // Access the related model
```

## Security

If you discover any security related issues, please email eric.tromas@gmail.com instead of using the issue tracker.

## Credits

- Inspired by Sander van Hooft[https://github.com/sandervanhooft]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/eric-tromas/eloquent-invoice.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/eric-tromas/eloquent-invoice.svg?style=flat-square

[link-author]: https://bitbucket.org/etromas/eloquent-invoice
