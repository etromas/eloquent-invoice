<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>@lang('invoice::invoice.Invoice') {{$invoice->reference}}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            background: #fff;
            background-image: none;
            font-size: 12px;
        }

        address {
            margin-top: 15px;
        }

        h2 {
            font-size: 28px;
            color: #cccccc;
        }

        .container {
            padding-top: 30px;
        }

        .invoice-head td {
            padding: 0 8px;
        }

        .invoice-body {
            background-color: transparent;
        }

        .logo {
            padding-bottom: 10px;
        }

        .table th {
            vertical-align: bottom;
            font-weight: bold;
            padding: 8px;
            line-height: 20px;
            text-align: left;
        }

        .table td {
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border-top: 1px solid #dddddd;
        }

        .well {
            margin-top: 15px;
        }
    </style>
</head>

<body>
<div class="container">
    <table style="margin-left: 0; margin-right: 0" width="500" border="0">
        <tr>
            <td width="160">
                &nbsp;
            </td>

            <!-- Sender information -->
            <td align="right">
                <strong>{{ $invoice->sender_info }}</strong>
            </td>
        </tr>
        <tr valign="top">
            <td style="font-size:28px;color:#cccccc;">
                @lang('invoice::invoice.Invoice')
            </td>

            <!-- Receiver information -->
            <td>
                <br><br>
                <strong>To:</strong> {{ $invoice->receiver_info }}
                <br>
                <strong>@lang('invoice::invoice.Date'):</strong> {{ $invoice->created_at }}
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
                <!-- Invoice Info -->
                <p>
                    <strong>Invoice Reference:</strong> {{ $invoice->reference }}<br>
                </p>

                <br><br>

                <!-- Invoice Table -->
                <table width="100%" class="table" border="0">
                    <tr>
                        <th align="left">@lang('invoice::invoice.Description')</th>
                        <th align="right">@lang('invoice::invoice.Unit price')</th>
                        <th align="right">@lang('invoice::invoice.Quantity')</th>
                        <th align="right">@lang('invoice::invoice.Tax %')</th>
                        <th align="right">@lang('invoice::invoice.Total excluded tax')</th>
                        <th align="right">@lang('invoice::invoice.Total included tax')</th>
                    </tr>

                    <!-- Display The Invoice Items -->
                    @foreach ($invoice->lines as $line)
                        <tr>
                            <td>{{ $line->description }}</td>
                            <td>{{ $moneyFormatter->format($line->unit_price_excluded_tax) }}</td>
                            <td>{{ $line->qty }}</td>
                            <td>{{ $line->tax_percentage * 100 }}%</td>
                            <td>{{ $moneyFormatter->format($line->total_excluded_tax) }}</td>
                            <td>{{ $moneyFormatter->format($line->total_included_tax) }}</td>
                        </tr>
                @endforeach

                <!-- Display The Final Total -->
                    <tr style="border-top:2px solid #000;">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td style="text-align: right;"><strong>Total</strong></td>
                        <td><strong>{{ $moneyFormatter->format($invoice->total_included_tax) }}</strong></td>
                    </tr>

                    <!-- Display The Tax specification -->
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td style="text-align: right;">Included tax</td>
                        <td>{{ $moneyFormatter->format($invoice->tax) }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="160">
                &nbsp;
            </td>

            <!-- Note -->
            <td align="right">
                <strong>{{ $invoice->note }}</strong>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
