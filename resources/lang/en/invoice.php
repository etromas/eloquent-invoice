<?php

return array(
    "Description" => "Description",
    "Invoice" => "Invoice",
    "Date" => "Date",
    "Unit price" => "Unit price",
    "Quantity" => "Quantity",
    "Tax %" => "Tax %",
    "Total excluded tax" => "Total excluded tax",
    "Total included tax" => "Total included tax"
);
