<?php

return array(
    "Description" => "Description",
    "Invoice" => "Facture",
    "Date" => "Date",
    "Unit price" => "Prix unitaire",
    "Quantity" => "Quantité",
    "Tax %" => "Taxes (%)",
    "Total excluded tax" => "Total HT",
    "Total included tax" => "Total TTC"
);
