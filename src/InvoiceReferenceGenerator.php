<?php

namespace EricTromas\EloquentInvoice;

class InvoiceReferenceGenerator
{
    public static function generate()
    {
        return date('Y-m-d') . '-' . self::generateRandomCode();
    }

    protected static function generateRandomCode()
    {
        $pool = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        return substr(str_shuffle(str_repeat($pool, 6)), 0, 6);
    }
}
