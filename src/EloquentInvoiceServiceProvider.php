<?php

namespace EricTromas\EloquentInvoice;

use Illuminate\Support\ServiceProvider;

class EloquentInvoiceServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $sourceViewsPath = __DIR__ . '/../resources/views';
        $this->loadViewsFrom($sourceViewsPath, 'eloquentInvoice');

        $this->publishes([
            $sourceViewsPath => resource_path('views/vendor/eloquentInvoice'),
        ], 'views');

        // Publish a config file
        $this->publishes([
            __DIR__ . '/../config/eloquentInvoice.php' => config_path('eloquentInvoice.php'),
        ], 'config');

        // Publish migrations
        $this->publishes([
            __DIR__ . '/../database/migrations/2017_06_17_163005_create_invoices_tables.php'
            => database_path(sprintf('migrations/%s_create_invoices_tables.php', date('Y_m_d_His'))),
        ], 'migrations');

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'invoice');
        $this->publishes([__DIR__ .'/../resources/lang' => resource_path('lang/vendor/eloquentInvoice')], 'translations');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/eloquentInvoice.php', 'eloquentInvoice');
    }
}
