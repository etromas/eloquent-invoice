<?php

namespace EricTromas\EloquentInvoice;

use Dompdf\Dompdf;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Invoice
 * @package EricTromas\EloquentInvoice
 */
class Invoice extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return mixed
     */
    public function getTotalIncludedTaxAttribute()
    {
        return $this->lines()->sum(DB::raw('(unit_price_excluded_tax * qty) + (unit_price_excluded_tax * qty * tax_percentage)'));
    }

    /**
     * @return mixed
     */
    public function getTotalExcludedTaxAttribute()
    {
        return $this->lines()->sum(DB::raw('unit_price_excluded_tax * qty'));
    }

    /**
     * @return mixed
     */
    public function getTaxAttribute()
    {
        return $this->lines()->sum(DB::raw('unit_price_excluded_tax * qty * tax_percentage'));
    }

    /**
     * @var array
     */
    protected $appends = ['total_included_tax', 'tax', 'total_excluded_tax'];

    /**
     * Get the invoice lines for this invoice
     */
    public function lines()
    {
        return $this->hasMany(InvoiceLine::class);
    }

    /**
     * @return mixed
     */
    public function invoicable()
    {
        return $this->morphTo();
    }

    /**
     * Use this if the amount does not yet include tax.
     * @param Int $unit_price_excluded_tax The amount in cents, excluding taxes
     * @param String $description The description
     * @param Int $qty The item quantity
     * @param Float $taxPercentage The tax percentage. Defaults to the config key tax
     * @return Invoice  This instance after recalculation
     */
    public function addLine($invoicable_type, $invoicable_id, $unit_price_excluded_tax, $description, $qty, $taxPercentage = null)
    {
        $taxPercentage = $taxPercentage ?? config('invoicable.default_tax');
        $this->lines()->create([
            'invoicable_item_type' => $invoicable_type,
            'invoicable_item_id' => $invoicable_id,
            'unit_price_excluded_tax' => $unit_price_excluded_tax,
            'description' => $description,
            'qty' => $qty,
            'tax_percentage' => $taxPercentage,
        ]);
        return $this;
    }

    /**
     * Get the View instance for the invoice.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\View\View
     */
    public function view(array $data = [])
    {
        return View::make('eloquentInvoice::receipt', array_merge($data, [
            'invoice' => $this,
            'moneyFormatter' => new MoneyFormatter(
                $this->currency,
                config('eloquentInvoice.locale', 'fr_FR')
            ),
        ]));
    }

    /**
     * Capture the invoice as a PDF and return the raw bytes.
     *
     * @param  array $data
     * @return string
     */
    public function pdf(array $data = [])
    {
        if (!defined('DOMPDF_ENABLE_AUTOLOAD')) {
            define('DOMPDF_ENABLE_AUTOLOAD', false);
        }

        if (file_exists($configPath = base_path() . '/vendor/dompdf/dompdf/dompdf_config.inc.php')) {
            require_once $configPath;
        }

        $dompdf = new Dompdf;
        $dompdf->loadHtml($this->view($data)->render());
        $dompdf->render();
        return $dompdf->output();
    }

    /**
     * Create an invoice download response.
     *
     * @param  array $data
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function download(array $data = [])
    {
        $filename = $this->reference . '.pdf';

        return new Response($this->pdf($data), 200, [
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Type' => 'application/pdf',
        ]);
    }

    /**
     * @param $reference
     * @return mixed
     */
    public static function findByReference($reference)
    {
        return static::where('reference', $reference)->first();
    }

    /**
     * @param $reference
     * @return mixed
     */
    public static function findByReferenceOrFail($reference)
    {
        return static::where('reference', $reference)->firstOrFail();
    }

    /**
     *
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->currency = config('invoicable.default_currency', 'EUR');
            $model->status = config('invoicable.default_status', 'draft');
            $model->reference = InvoiceReferenceGenerator::generate();
        });
    }
}
