<?php

namespace EricTromas\EloquentInvoice;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InvoiceLine
 * @package EricTromas\EloquentInvoice
 */
class InvoiceLine extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return mixed
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * @return mixed
     */
    public function invoicable_item()
    {
        return $this->morphTo();
    }

    /**
     * @var array
     */
    protected $appends = ['total_excluded_tax', 'total_included_tax', 'tax'];

    /**
     * @return float|int
     */
    public function getTotalExcludedTaxAttribute()
    {
        return $this->unit_price_excluded_tax * $this->qty;
    }

    /**
     * @return float|int
     */
    public function getTotalIncludedTaxAttribute()
    {
        return $this->unit_price_excluded_tax * $this->qty + $this->tax;
    }

    /**
     * @return float|int
     */
    public function getTaxAttribute()
    {
        return $this->unit_price_excluded_tax * $this->qty * $this->tax_percentage;
    }

}
