<?php

namespace EricTromas\EloquentInvoice\IsInvoicable;

use EricTromas\EloquentInvoice\InvoiceLine;

trait IsInvoicableItemTrait
{
    /**
     * Set the polymorphic relation.
     *
     * @return mixed
     */
    public function invoices()
    {
        return $this->morphMany(InvoiceLine::class, 'invoicable_item')->invoice();
    }
}
